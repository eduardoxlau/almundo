# config valid only for current version of Capistrano
lock "3.10.1"

set :application, "almundo"
set :repo_url, "git@gitlab.com:eduardoxlau/almundo.git"
set :deploy_to, "/home/eduardoxlau92"
set :linked_dirs, %w{log node_modules tmp cache}
set :linked_dirs, fetch(:linked_dirs, []) << "cache"
# set :pty, true

namespace :deploy do

  desc 'Copy upstart script'
  task :upstart do
    on roles(:app) do
      within release_path do
        sudo :cp, "etc/#{fetch :application}.upstart.conf", "/etc/init/#{fetch :application}.conf"
      end
    end
  end

  desc 'Restart application'
  task :restart do
    on roles(:app) do
      sudo :service, 'nginx restart'
    end
  end

  desc 'Install node modules'
  task :install_node_modules do
    on roles(:app) do
      within release_path do
        execute :npm, 'install', '-s'
      end
    end
  end


  after :updated, :install_node_modules
  ##after :updated, :upstart
  after :publishing, :restart

end