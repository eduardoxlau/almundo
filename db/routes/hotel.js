//////////////////////////////////////////////////////// 
//  Route hotel  Express
//  Project: almundo
//  Created by Rafael eduardo sanchez paz on 03/02/18.
//////////////////////////////////////////////////////// 
const express = require('express');
const router = express.Router();

const Hotel =require('../models/hotel');

router.post('/hotel',(req,res)=>{
	var hotel = new Hotel({
		name: req.body.name,
		stars: req.body.stars,
		price: req.body.price,
		image: req.body.image
	});
	hotel.save(function(err){
		if(!err){
			res.status(201).send({results:hotel});
		}
		else res.status(500).send({results:"fail"});
	});
});
router.post('/hotel/update',(req,res)=>{
	Hotel.findById(req.body.id,(err,hotel)=>{
		if(req.body.name)hotel.name = req.body.name;
		if(req.body.stars)hotel.stars = req.body.stars;
		if(req.body.price)hotel.price = req.body.price;
		if(req.body.image)hotel.image=req.body.image;
		hotel.save(function(err){
			if(!err)res.status(200).send({results:"ok"});
			else res.status(400).send({results:"fail"});
		});
	});	
});
router.post('/hotel/remove',(req,res)=>{
	Hotel.findByIdAndRemove(req.body.id, (err) => {  
		if(!err)res.status(200).send({results:"ok"});
		else res.status(400).send({results:"fail"});
	});
});
router.post('/hotel/amenities',(req,res)=>{
	Hotel.findById(req.body.id, (err, hotel) => {  
		hotel.amenities.push(req.body.amenitie);
		hotel.save((err)=>{
			if(!err)res.status(200).send({results:"ok"});
			else res.status(400).send({results:"fail"});
		});
		
	});
});
router.get('/hotels',(req,res)=>{
	Hotel.find((err,hotels)=>{
		if(!err)res.status(200).send({results:hotels});
		else
			res.status(400).send({results:err});
	})
});
router.get('/hotels/:id',(req,res)=>{
	Hotel.findById(req.params.id,(err,hotel)=>{
		if(!err)res.status(200).send({results:hotel});
		else res.status(400).send({results:err});
	})
});
// router.post('/filter',(req,res)=>{

// 	Hotel.search({query_string: {query: req.body.value }},{hydrate:true}, (err, results)=> {
// 		var objects=[];
// 		objects=results.hits.hits
// 		objects = objects.filter(function( element ) {
// 		   return element !== undefined;
// 		});
		
// 		if(!err && objects){

// 			res.status(400).send({results: objects});
// 		}
// 		else res.status(400).send({results: null});

		
		
// 	});
// });
router.post('/filter',(req,res)=>{
	if(req.body.search=='null'){
		console.log(req.body.data);
		var index=req.body.data.indexOf('0');
		console.log(index);
	    if(index==1 || index==0)req.body.data=[1,2,3,4,5];
		Hotel.find({stars: {$in: req.body.data}},(err,hotels)=>{
			if(!err){
				//console.log(hotels)
				res.status(200).send({results:hotels})
			}
			else {
				//console.log(err);
				res.status(400).send({results:err});
			}

		})	
	}else{
		console.log(req.body);
		Hotel.search({query_string: {query: req.body.search }},{hydrate:true}, (err, results)=> {
			var objects=[];
			objects=results.hits.hits
			objects = objects.filter(function( element ) {
			   return element !== undefined;
			});
			
			if(!err && objects){
				console.log(objects);
				res.status(200).send({results: objects});
			}
			else{
				console.log(err);
				res.status(400).send({results: null});
			}

			
			
		});
	}
	
});
module.exports = router;