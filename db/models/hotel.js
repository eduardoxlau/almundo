//////////////////////////////////////////////////////// 
//  Model hotel  Express
//  Project: almundo
//  Created by Rafael eduardo sanchez paz on 03/02/18.
//////////////////////////////////////////////////////// 
const mongoose=require('mongoose'),
   mongoosastic = require('mongoosastic'),
	mongoosePaginate = require('mongoose-paginate');
	Schema = mongoose.Schema;

const hotel = new Schema({
		name: { type:String, es_indexed:true},
		stars: { type:Number},
		price: {type:String},
		image:{type:String},
		amenities:[{type:String}]
	},
	{timestamps: { createdAt: 'created_at' }});
hotel.plugin(mongoosePaginate);
hotel.plugin(mongoosastic);
var Hotel= mongoose.model('Hotel',hotel)
  , stream = Hotel.synchronize()
  , count = 0;
stream.on('data', function(err, doc){
  count++;
});
stream.on('close', function(){
  console.log('indexed ' + count + ' documents!');
});
stream.on('error', function(err){
  console.log(err); 
});
module.exports =Hotel;