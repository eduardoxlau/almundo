## Almundo backend

this project provide api rest for a hotel appication,it use nodejs, mongodb, elasticsearch, you can download el frontend made in angular here *https://gitlab.com/eduardoxlau/almundoFrontend ,for visit deployed project here *http://104.196.97.170
## Apis rest


description: create new hotel <br>
method:post<br>
url: localhost:3000/api/hotel<br>
parameters: name,image,stars,price<br>

description: Edit  hotel<br>
method:post<br>
url: localhost:3000/api/hotel/update<br>
parameters: name,image,stars,price<br>

description: delete hotel<br>
method:post<br>
url: localhost:3000/api/hotel/remove<br>
parameters: id<br>

## install package
run `npm install`

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:3000/`. The app will automatically reload if you change any of the source files.
