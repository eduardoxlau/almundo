//////////////////////////////////////////////////////// 
//  index main hotel  Express
//  Project: almundo
//  Created by Rafael eduardo sanchez paz on 03/02/18.
//////////////////////////////////////////////////////// 
const express= require('express');
const app= express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const server=require('http').Server(app);
app.use(express.static('public'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*'); // * => allow all origins
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,OPTIONS,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type, X-Auth-Token, Accept,authorization'); // add remove headers according to your needs
  next()
});
//******** configuration  mongodb  conected *******************
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/almundo',function(err,res){
	if(err) console.log("Erro:conectando a la base de datos"+err);
	else console.log("conectado exitosamente");
});
const hotel = require('./db/routes/hotel');
app.use('/api', hotel);
server.listen(3000,() =>{console.log("express run!!")});

